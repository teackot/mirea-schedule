import gi
gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")
from gi.repository import Gtk, Adw

from datetime import date, timedelta

from schedule import Schedule


WEEKDAYS = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]


class ScheduleWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwarg):
        super().__init__(*args, **kwarg)

        self.schedule = Schedule("schedule.json")
        self.todays_week = self.schedule.get_weeknum(date.today())
        self.current_week = self.todays_week

        self.set_title("МИРЭА")
        self.set_default_size(850, 600)

        # Header bar

        self.header = Gtk.HeaderBar()

        self.header_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.header_box.set_spacing(8)
        self.header.pack_start(self.header_box)
        
        ## Previous & next week
        
        self.week_chooser_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        # self.week_chooser_box.set_spacing(10)
        self.week_chooser_box.set_homogeneous(True)
        self.header_box.append(self.week_chooser_box)

        self.button_left = Gtk.Button()
        self.button_left.set_icon_name("go-previous-symbolic")
        self.button_left.set_sensitive(False if self.current_week == 1 else True)
        self.button_left.connect("clicked", self.on_button_left_clicked)
        self.week_chooser_box.append(self.button_left)

        self.week_lbl = Gtk.Label(label=str(self.current_week))
        self.week_chooser_box.append(self.week_lbl)
        
        self.button_right = Gtk.Button()
        self.button_right.set_icon_name("go-next-symbolic")
        self.button_right.connect("clicked", self.on_button_right_clicked)
        self.week_chooser_box.append(self.button_right)

        ## Today button

        self.today_button = Gtk.Button(label="Сегодня")
        self.today_button.connect("clicked", self.on_today_button_clicked)
        self.today_button.set_visible(False)
        self.header_box.append(self.today_button)

        # Set titlebar
        
        self.set_titlebar(self.header)

        # Main layout
        
        self.set_child(self.generate_view(self.current_week))

    def generate_view(self, week: int) -> Gtk.Widget:
        scrolled_window = Gtk.ScrolledWindow()

        clamp = Adw.Clamp()
        clamp.set_maximum_size(600)
        clamp.set_tightening_threshold(400)
        scrolled_window.set_child(clamp)
        
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        main_box.set_margin_start(12)
        main_box.set_margin_end(12)
        main_box.set_margin_top(12)
        main_box.set_margin_bottom(12)
        main_box.set_spacing(12)
        clamp.set_child(main_box)

        # Weekday groups

        monday_date = self.schedule.get_first_week_start() + timedelta(weeks=(self.current_week - 1))

        for i in range(7):
            day_date = monday_date + timedelta(days=i)
            
            group = Adw.PreferencesGroup()
            group.set_title(f"{WEEKDAYS[i]}, {str(day_date.day).zfill(2)}.{str(day_date.month).zfill(2)}")
            # TODO: date

            entries = self.schedule.get_weekday_entries(week, i)
            if len(entries) == 0:
                # Empty day
                row = Adw.ActionRow()
                row.set_title("Пар нет!")
                row.set_subtitle("-----")
                group.add(row)
            else:
                for entry in entries:
                    # set the row up
                    row = Adw.ExpanderRow()
                    row.set_title(entry.name)
                    row.set_subtitle(f"{entry.start_time} - {entry.end_time}")

                    # fill the row
                    
                    campus_row = Adw.ActionRow()
                    campus_row.set_title("Кампус")
                    campus_row.add_suffix(Gtk.Label(label=entry.campus))
                    row.add_row(campus_row)
                    
                    room_row = Adw.ActionRow()
                    room_row.set_title("Аудитория")
                    room_row.add_suffix(Gtk.Label(label=entry.room))
                    row.add_row(room_row)

                    type_row = Adw.ActionRow()
                    type_row.set_title("Вид занятий")
                    type_row.add_suffix(Gtk.Label(label=entry.type))
                    row.add_row(type_row)
                    
                    group.add(row)
            
            main_box.append(group)
        
        return scrolled_window

    def on_button_left_clicked(self, button):
        if self.current_week > 1:
            self.current_week -= 1
            self.week_lbl.set_label(str(self.current_week))
            self.set_child(self.generate_view(self.current_week))
            
            if self.current_week == 1:
                button.set_sensitive(False)
            if self.current_week == self.todays_week:
                self.today_button.set_visible(False)
            else:
                self.today_button.set_visible(True)
    
    def on_button_right_clicked(self, button):
        self.current_week += 1
        self.week_lbl.set_label(str(self.current_week))
        self.set_child(self.generate_view(self.current_week))
        
        self.button_left.set_sensitive(True)
        if self.current_week == self.todays_week:
            self.today_button.set_visible(False)
        else:
            self.today_button.set_visible(True)

    def on_today_button_clicked(self, button):
        self.todays_week = self.schedule.get_weeknum(date.today())
        self.current_week = self.todays_week
        self.week_lbl.set_label(str(self.current_week))
        self.set_child(self.generate_view(self.current_week))
        
        if self.current_week == 1:
            self.button_left.set_sensitive(False)
        else:
            self.button_left.set_sensitive(True)
        self.today_button.set_visible(False)

