import sys
from application import ScheduleApplication

if __name__ == '__main__':
    app = ScheduleApplication(application_id='com.gitlab.teackot.mirea-schedule')
    app.run(sys.argv)

