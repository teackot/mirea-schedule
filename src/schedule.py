import json
from datetime import date, datetime

PAIR_TIMES = [
    [ "9:00", "10:30"],
    ["10:40", "12:10"],
    ["12:40", "14:10"],
    ["14:20", "15:50"],
    ["16:20", "17:50"],
    ["18:00", "19:30"],
]


class Entry:
    def __init__(self, name: str, start_time: str, end_time: str, type: str, room: str, campus: str):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.type = type
        self.room = room
        self.campus = campus


class Schedule:
    def __init__(self, schedule_file_path: str):
        f = open(schedule_file_path)
        
        self.sch = json.load(f)
        self._first_week_start = date.fromisoformat(self.sch["first_week_start"])

        f.close()

    def get_first_week_start(self) -> date:
        return self._first_week_start

    def get_weeknum(self, d: date):
        delta = d - self._first_week_start
        return (delta.days // 7) + 1

    def get_weekday_entries(self, weeknum: int, weekday: int):
        entries = list()
        day_sch = self.sch["schedule"][weekday]["odd" if weeknum % 2 == 1 else "even"]
        
        if len(day_sch) > 0:
            first_pair_index = day_sch[0]["first_pair_index"]
            for i in range(len(day_sch)):
                day = day_sch[i]

                _keys = day.keys()
                if "except_weeks" in _keys:
                    if weeknum in day["except_weeks"]:
                        continue
                if "only_weeks" in _keys:
                    if weeknum not in day["only_weeks"]:
                        continue
                
                pair_index = first_pair_index + i
                
                name       = day["name"]
                start_time = PAIR_TIMES[pair_index][0]
                end_time   = PAIR_TIMES[pair_index][1]
                type       = str()
                room       = day["room"]
                campus     = str()

                match day["type"]:
                    case "lec":
                        type = "лекция"
                    case "prac":
                        type = "практика"
                    case "lab":
                        type = "лабораторная работа"
                    case _:
                        type = "unknown"

                match day["campus"]:
                    case "С-20":
                        campus = "Стромынка 20"
                    case "В-78":
                        campus = "Вернадского 78"
                    case "В-86":
                        campus = "Вернадского 86"
                    case "СГ-22":
                        campus = "Соколиная гора 22"
                    case "МП-1":
                        campus = "Малая Пироговская 1"
                    case _:
                        campus = "unknown"
    
                entries.append(Entry(name, start_time, end_time, type, room, campus))
            
        return entries

